import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppComponent } from './app.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SquatsServiceService} from '../services/squats-service/squats-service.service';
import {HttpModule} from '@angular/http';
import { ModalModule } from 'ngx-bootstrap/modal';

import { Http } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    NgbModule.forRoot(),
    TabsModule.forRoot(),
    HttpModule,
    ModalModule.forRoot()
  ],
  providers: [SquatsServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
