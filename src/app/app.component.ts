import { Component, OnInit, TemplateRef } from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import { SquatsServiceService} from '../services/squats-service/squats-service.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  data:any;
  commentCount:any;
  photoCount:any;
  albumCount:any;
  userData:any;
  userTasks:any;
  modalRef: BsModalRef;
  constructor(public _data:SquatsServiceService,private modalService: BsModalService) {
    
  }
  getHome() {
    this._data.getAllPosts().subscribe(comments=>{
      this.data=comments;
    });
  }
  getAlbum() {
    this.data={}
    this._data.getAllAlbum().subscribe(comments=>{
      this.data=comments;
    });
  }
  getPhotos() {
    this.data={}
    this._data.getAllPhotos().subscribe(comments=>{
      this.data=comments;
      console.log(this.data);
    });
  }
  getUsers() {
    this.data={}
    this._data.getAllUsers().subscribe(comments=>{
      this.data=comments;
    });
  }

  getCommentCount() {
    this._data.getCommentCount().subscribe(comments=>{
      this.commentCount=comments;
      this.commentCount=this.commentCount.length;
    });

  }
  getPhotoCount() {
    this._data.getPhotosCount().subscribe(comments=>{
      this.photoCount=comments;
      this.photoCount=this.photoCount.length;
    });
  }
  getAlbumCount() {
    this._data.getAlbumCount().subscribe(comments=>{
      this.albumCount=comments;
      this.albumCount=this.albumCount.length;
    });
  }

  getUserList() {
    this._data.getAllUsers().subscribe(comments=>{
      this.userData=comments;
    });
  }

  getToDoList() {
    this._data.getAllTasks().subscribe(comments=>{
      this.userTasks=comments;
    });
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  toaster() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

  ngOnInit() {
   this.getHome();
   this.getAlbumCount();
   this.getPhotoCount();
   this.getCommentCount();
   this.getUserList();
   this.getToDoList();
  }
}
