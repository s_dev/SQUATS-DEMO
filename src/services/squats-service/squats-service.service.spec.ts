import { TestBed, inject } from '@angular/core/testing';

import { SquatsServiceService } from './squats-service.service';

describe('SquatsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SquatsServiceService]
    });
  });

  it('should be created', inject([SquatsServiceService], (service: SquatsServiceService) => {
    expect(service).toBeTruthy();
  }));
});
