import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class SquatsServiceService {

  constructor(public http: Http) {

  }

  getCommentCount() {
    return this.http.get('https://jsonplaceholder.typicode.com/comments')
      .map(comments => comments.json());
  }

  getPhotosCount() {
    return this.http.get('https://jsonplaceholder.typicode.com/photos')
      .map(photos => photos.json());
  }

  getAlbumCount() {
    return this.http.get('https://jsonplaceholder.typicode.com/albums')
      .map(album => album.json());
  }

  getAllPosts() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
      .map(res => res.json());
  }

  getAllAlbum() {
    return this.http.get('https://jsonplaceholder.typicode.com/albums')
      .map(res => res.json());
  }

  getAllPhotos() {
    return this.http.get('https://jsonplaceholder.typicode.com/photos')
      .map(res => res.json());
  }

  getAllUsers() {
    return this.http.get('https://jsonplaceholder.typicode.com/users')
      .map(users => users.json());
  }

  getAllTasks() {
    return this.http.get('https://jsonplaceholder.typicode.com/todos')
      .map(tasks => tasks.json());
  }

  addUsers(user) {
    this.http.post('https://jsonplaceholder.typicode.com/users', user)
      .map(tasks => tasks.json());
  }

}
